package eu.su.mas.dedaleEtu.mas.agents.yours;

import java.util.ArrayList;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.ExploMultiBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.TankerBehaviour;
import jade.core.behaviours.Behaviour;

public class TankerAgent extends AbstractDedaleAgent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected void setup(){

		super.setup();
		List<Behaviour> lb=new ArrayList<Behaviour>();
		final Object[] args=getArguments();
		if(args.length!=3) {
			System.err.println("Malfunction - 1 argument expected");
			System.exit(-1);
		}else {
			List<String> nameList=(ArrayList<String>) args[2];
			lb.add(new TankerBehaviour(this, nameList));
			addBehaviour(new startMyBehaviours(this,lb));
			System.out.println("the  agent "+this.getLocalName()+ " is started");
		}
	}


}
