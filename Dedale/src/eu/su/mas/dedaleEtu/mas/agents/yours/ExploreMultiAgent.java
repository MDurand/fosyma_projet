package eu.su.mas.dedaleEtu.mas.agents.yours;

import java.util.ArrayList;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.ExploMultiBehaviour;
import jade.core.behaviours.Behaviour;

public class ExploreMultiAgent extends AbstractDedaleAgent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	protected void setup(){

		super.setup();
		List<Behaviour> lb=new ArrayList<Behaviour>();
		final Object[] args=getArguments();
		if(args.length!=3) {
			System.err.println("Malfunction - 1 argument expected");
			System.exit(-1);
		}else {
			List<String> nameList=(ArrayList<String>) args[2];
			lb.add(new ExploMultiBehaviour(this, nameList));
			addBehaviour(new startMyBehaviours(this,lb));
			System.out.println("the  agent "+this.getLocalName()+ " is started");
		}
	}
}
