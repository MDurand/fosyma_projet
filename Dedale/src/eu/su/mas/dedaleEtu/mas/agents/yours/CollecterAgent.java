package eu.su.mas.dedaleEtu.mas.agents.yours;

import java.util.ArrayList;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.CollectBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.ExploMultiBehaviour;
import jade.core.behaviours.Behaviour;

public class CollecterAgent extends AbstractDedaleAgent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected void setup(){
		super.setup();
		List<Behaviour> lb=new ArrayList<Behaviour>();
		final Object[] args=getArguments();
		if(args.length!=4) {
			System.err.println("Malfunction - 2 arguments expected");
			System.exit(-1);
		}else {
			List<String> nameList=(ArrayList<String>) args[2];
			String nameTanker=(String) args[3];
			lb.add(new CollectBehaviour(this, nameList, nameTanker));
			addBehaviour(new startMyBehaviours(this,lb));
			System.out.println("the  agent "+this.getLocalName()+ " is started");
		}
	}

}
