package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.ArrayList;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class ReceiveGoldInfoBehaviour extends SimpleBehaviour{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;
	
	private ArrayList<ArrayList<String>> goldNodes;
	private ArrayList<ArrayList<String>> diamondNodes;
	private ArrayList<String> emptyNodes;
	
	public ReceiveGoldInfoBehaviour(AbstractDedaleAgent myAgent, ArrayList<ArrayList<String>> goldNodes, ArrayList<ArrayList<String>> diamondNodes, ArrayList<String> emptyNodes) {
		super(myAgent);
		this.goldNodes=goldNodes;
		this.diamondNodes=diamondNodes;
		this.emptyNodes=emptyNodes;
	}

	@Override
	public void action() {
		final MessageTemplate msgTemplateGold = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),MessageTemplate.MatchProtocol("MAPINFOGOLD"));
		final ACLMessage msgGold = this.myAgent.receive(msgTemplateGold);
		if(msgGold != null) {
			ArrayList<ArrayList<String>> gold;
			try {
				gold = (ArrayList<ArrayList<String>>) msgGold.getContentObject();
				for(ArrayList<String> a : gold) {
					boolean known=false;
					String node=a.get(0);
					for(ArrayList<String> b : goldNodes) {
						if(b.get(0).equals(node)) {
							known=true;
							break;
						}
					}
					if(!known && !emptyNodes.contains(node)) {
						goldNodes.add(a);
					}
				}
			} catch (UnreadableException e) {
				e.printStackTrace();
			}
		}
		final MessageTemplate msgTemplateDiamond = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),MessageTemplate.MatchProtocol("MAPINFODIAMOND"));
		final ACLMessage msgDiamond = this.myAgent.receive(msgTemplateDiamond);
		if(msgDiamond != null) {
			ArrayList<ArrayList<String>> diamond;
			try {
				diamond = (ArrayList<ArrayList<String>>) msgDiamond.getContentObject();
				for(ArrayList<String> a : diamond) {
					boolean known=false;
					String node=a.get(0);
					for(ArrayList<String> b : diamondNodes) {
						if(b.get(0).equals(node)) {
							known=true;
							break;
						}
					}
					if(!known && !emptyNodes.contains(node)) {
						diamondNodes.add(a);
					}
				}
			} catch (UnreadableException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean done() {
		return finished;
	}

}
