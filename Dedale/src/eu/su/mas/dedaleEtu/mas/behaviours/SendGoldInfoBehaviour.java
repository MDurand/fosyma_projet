package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;

public class SendGoldInfoBehaviour extends SimpleBehaviour {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;

	private ArrayList<ArrayList<String>> goldNodes;
	private ArrayList<ArrayList<String>> diamondNodes;

	private List<String> receivers;

	public SendGoldInfoBehaviour(AbstractDedaleAgent myAgent, ArrayList<ArrayList<String>>goldNodes, ArrayList<ArrayList<String>>diamondNodes,List<String> receivers) {
		super(myAgent);
		this.goldNodes=goldNodes;
		this.diamondNodes=diamondNodes;
		this.receivers=receivers;
	}
	
	@Override
	public void action() {
		//Création du messages
		final ACLMessage msgGold = new ACLMessage(ACLMessage.INFORM);
		final ACLMessage msgDiamond = new ACLMessage(ACLMessage.INFORM);
				
		//Ajout de l'envoyeur
		msgGold.setSender(this.myAgent.getAID());
		msgDiamond.setSender(this.myAgent.getAID());
		
		//Ajout des recepteurs
		for(String receiver : receivers) {
			msgGold.addReceiver(new AID(receiver, AID.ISLOCALNAME));
			msgDiamond.addReceiver(new AID(receiver, AID.ISLOCALNAME));
		}
		
		//Définition des protocoles
		msgGold.setProtocol("MAPINFOGOLD");
		msgDiamond.setProtocol("MAPINFODIAMOND");
		
		//Ajout du contenu
		try {
			msgGold.setContentObject(goldNodes);
			msgDiamond.setContentObject(diamondNodes);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		//Envoi
		if(this.myAgent!=null) {
			try {
				((AbstractDedaleAgent) this.myAgent).sendMessage(msgGold);
				((AbstractDedaleAgent) this.myAgent).sendMessage(msgDiamond);
			}catch(NullPointerException e) {
			}
		}
		finished=true;
	}

	@Override
	public boolean done() {
		return finished;
	}
}
