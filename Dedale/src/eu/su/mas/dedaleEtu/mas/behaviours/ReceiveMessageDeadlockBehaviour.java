package eu.su.mas.dedaleEtu.mas.behaviours;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ReceiveMessageDeadlockBehaviour extends SimpleBehaviour{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;
	private String obj;
	private IDeadlockMultiAgentBehaviour b;
	
	public ReceiveMessageDeadlockBehaviour(final AbstractDedaleAgent myAgent, String obj, IDeadlockMultiAgentBehaviour b) {
		super(myAgent);
		this.obj=obj;
		this.b=b;
	}

	@Override
	public void action() {
		final MessageTemplate msgTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
																MessageTemplate.MatchProtocol("DEADLOCK"));			

		final ACLMessage msg = this.myAgent.receive(msgTemplate);
		if (msg != null) {	
			String[] tab=msg.getContent().split(",");
			String position2=tab[0];
			String positionCible2=tab[1];
			String id2=tab[2];
			String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
			if(positionCible2.equals(myPosition) || obj.equals(position2)) {
				String myId=((AbstractDedaleAgent)this.myAgent).getAID().toString();
				if(myId.compareTo(id2)<=0 || b instanceof TankerBehaviour) {
					this.b.changeDestination();
				}
			}
			finished=true;
		}
	}

	@Override
	public boolean done() {
		return finished;
	}

}
