package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;

public class AskHelpChestBehaviour extends SimpleBehaviour{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;
	
	private List<String> receivers;
	private String node;
	private Integer str;
	private Integer lockPick;
	
	AbstractDedaleAgent myAgent;
	
	public AskHelpChestBehaviour(AbstractDedaleAgent myAgent, String n, Integer s, Integer lp, List<String> receivers){
		super(myAgent);
		this.myAgent=myAgent;
		this.receivers=receivers;
		this.node=n;
		this.str=s;
		this.lockPick=lp;
	}

	@Override
	public void action() {
		final ACLMessage msgChest = new ACLMessage(ACLMessage.INFORM);
		msgChest.setSender(this.myAgent.getAID());
		for(String receiver : receivers) {
			msgChest.addReceiver(new AID(receiver, AID.ISLOCALNAME));
		}
		msgChest.setProtocol("HELPCHEST");
		msgChest.setContent(node+","+str+","+lockPick);
		((AbstractDedaleAgent) this.myAgent).sendMessage(msgChest);
		finished=true;
	}

	@Override
	public boolean done() {
		return finished;
	}

}
