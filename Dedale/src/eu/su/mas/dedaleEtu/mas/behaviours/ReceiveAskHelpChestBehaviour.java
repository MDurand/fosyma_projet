package eu.su.mas.dedaleEtu.mas.behaviours;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ReceiveAskHelpChestBehaviour extends SimpleBehaviour{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;
	
	private IChestOpeningBehaviour b;
	
	private Integer str;
	private Integer lp;
	
	private AbstractDedaleAgent myAgent;
	
	public ReceiveAskHelpChestBehaviour(AbstractDedaleAgent myAgent, Integer str, Integer lp, IChestOpeningBehaviour b) {
		super(myAgent);
		this.myAgent=myAgent;
		this.str=str;
		this.lp=lp;
		this.b=b;
	}

	@Override
	public void action() {
		final MessageTemplate msgTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
		MessageTemplate.MatchProtocol("HELPCHEST"));			

		final ACLMessage msg = this.myAgent.receive(msgTemplate);
		if (msg != null) {	
			String info[]=msg.getContent().split(",");
			Integer s=Integer.valueOf(info[1]);
			Integer l=Integer.valueOf(info[2]);
			if((s>0 && str>0)||(l>0 && lp>0)) {
				b.setObjective(info[0]);
			}
		}
	}

	@Override
	public boolean done() {
		return finished;
	}

}
