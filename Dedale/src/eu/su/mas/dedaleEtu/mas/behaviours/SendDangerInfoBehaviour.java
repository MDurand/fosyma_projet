package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;

public class SendDangerInfoBehaviour extends SimpleBehaviour {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;
	private ArrayList<String> dangerousNodes;
	private List<String> receivers;
	private AbstractDedaleAgent myAgent;
	
	public SendDangerInfoBehaviour(AbstractDedaleAgent myAgent, ArrayList<String> dangerousNodes, List<String> receivers) {
		super(myAgent);
		this.dangerousNodes=dangerousNodes;
		this.receivers=receivers;
		this.myAgent=myAgent;
	}
	@Override
	public void action() {
		final ACLMessage msgDanger = new ACLMessage(ACLMessage.INFORM);
		msgDanger.setSender(this.myAgent.getAID());
		for(String receiver : receivers) {
			msgDanger.addReceiver(new AID(receiver, AID.ISLOCALNAME));
		}
		msgDanger.setProtocol("MAPINFODANGER");
		try {
			msgDanger.setContentObject(dangerousNodes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((AbstractDedaleAgent) this.myAgent).sendMessage(msgDanger);
		finished=true;
	}

	@Override
	public boolean done() {
		return finished;
	}

}
