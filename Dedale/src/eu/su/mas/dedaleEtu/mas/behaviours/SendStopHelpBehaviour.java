package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;

public class SendStopHelpBehaviour extends SimpleBehaviour{
	private static final long serialVersionUID = 1L;
	private boolean finished;
	
	private List<String> receivers;
	AbstractDedaleAgent myAgent;
	
	public SendStopHelpBehaviour(AbstractDedaleAgent myAgent, List<String> receivers){
		super(myAgent);
		this.myAgent=myAgent;
		this.receivers=receivers;
	}

	@Override
	public void action() {
		final ACLMessage msgStop = new ACLMessage(ACLMessage.INFORM);
		msgStop.setSender(this.myAgent.getAID());
		for(String receiver : receivers) {
			msgStop.addReceiver(new AID(receiver, AID.ISLOCALNAME));
		}
		msgStop.setProtocol("STOPHELP");
		msgStop.setContent("STOP");
		((AbstractDedaleAgent) this.myAgent).sendMessage(msgStop);
		finished=true;
	}

	@Override
	public boolean done() {
		return finished;
	}


}
