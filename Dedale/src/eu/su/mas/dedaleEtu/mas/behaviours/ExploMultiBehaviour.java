package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.behaviours.SimpleBehaviour;

public class ExploMultiBehaviour extends SimpleBehaviour implements IDeadlockMultiAgentBehaviour{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;
	
	private ArrayList<String> openNodes;
	private HashSet<String> closedNodes;
	private HashSet<String> edges;
	
	private ArrayList<ArrayList<String>> goldNodes;
	private ArrayList<ArrayList<String>> diamondNodes;
	private ArrayList<String> dangerousNodes;
	private ArrayList<String> emptyNodes;
	
	private MapRepresentation myMap;

	private List<String> receivers;
	
	private AbstractDedaleAgent myAgent;
	
	private SendGoldInfoBehaviour sgib;
	private int count_deadlock;
	
	private ReceiveTankerPositionBehaviour rtpb;
	private String tankerPosition;
	
	
	public ExploMultiBehaviour(AbstractDedaleAgent myAgent, List<String> receivers) {
		super(myAgent);
		this.myAgent=myAgent;
		this.receivers=new ArrayList<>();
		for(String r : receivers) {
			this.receivers.add(r);
		}	
		openNodes=new ArrayList<>();
		closedNodes=new HashSet<>();
		edges=new HashSet<>();
		goldNodes=new ArrayList<>();
		diamondNodes=new ArrayList<>();
		dangerousNodes=new ArrayList<>();
		emptyNodes=new ArrayList<>();
		
		tankerPosition=null;
		
		count_deadlock=0;
	}
	
	@Override
	public void action() {
		if(this.myMap==null) {
			this.myMap= new MapRepresentation();
			myAgent.addBehaviour(new ReceiveMapInfoBehaviour(myAgent, myMap, openNodes, closedNodes,edges, dangerousNodes));
			myAgent.addBehaviour(new ReceiveGoldInfoBehaviour(myAgent, goldNodes, diamondNodes, emptyNodes));
			rtpb=new ReceiveTankerPositionBehaviour(myAgent);
			myAgent.addBehaviour(rtpb);
		}
		if(tankerPosition==null) {
			tankerPosition=rtpb.getPosition();
		}else {
			myAgent.addBehaviour(new SendTankerPositionBehaviour(myAgent, receivers, tankerPosition));
		}
		if(dangerousNodes.size()==openNodes.size() && closedNodes.size()>2) {
			for(String well : dangerousNodes) {
				openNodes.remove(well);
				closedNodes.add(well);
			}
		}
		myAgent.addBehaviour(new SendMapInfoBehaviour(myAgent, closedNodes, openNodes, edges, receivers));
		sgib=new SendGoldInfoBehaviour(myAgent, goldNodes, diamondNodes, receivers);
		myAgent.addBehaviour(new SendDangerInfoBehaviour(myAgent, dangerousNodes, receivers));
		myAgent.addBehaviour(sgib);
		//0) Retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		if (myPosition!=null){
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs;
			try {
				lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			}catch(ConcurrentModificationException e){
				lobs=new ArrayList<>();
			}
			/**
			 * Just added here to let you see what the agent is doing, otherwise he will be too quick
			 */
			try {
				this.myAgent.doWait(500);
			} catch (Exception e) {
				e.printStackTrace();
			}
			boolean wind=false;
			
			//1) remove the current node from openlist and add it to closedNodes.
			if(lobs.size()>0) {
				this.closedNodes.add(myPosition);
				this.openNodes.remove(myPosition);
				
				this.myMap.addNode(myPosition);
				for(Couple<String, List<Couple<Observation, Integer>>> c : lobs) {
					if(c.getLeft().equals(myPosition)) {
						for(Couple<Observation, Integer> C : c.getRight()) {
							if(C.getLeft().equals(Observation.WIND)) {
								wind=true;
							}
						}
						
					}
				}
			}
			//2) get the surrounding nodes and, if not in closedNodes, add them to open nodes.
			String nextNode=null;
			Iterator<Couple<String, List<Couple<Observation, Integer>>>> iter=lobs.iterator();
			while(iter.hasNext()){
				Couple<String, List<Couple<Observation, Integer>>> node=iter.next();
				String nodeId=node.getLeft();
				List<Couple<Observation, Integer>> obs =node.getRight();
				for(Couple<Observation, Integer> c : obs) {
					if(c.getLeft().equals(Observation.GOLD)) {
						addGoldInfo(node);
					}
					if(c.getLeft().equals(Observation.DIAMOND)) {
						addDiamondInfo(node);
					}
				}
				if (!this.closedNodes.contains(nodeId)){
					if (!this.openNodes.contains(nodeId)) {
						if(wind && !nodeId.equals(myPosition)){
							dangerousNodes.add(nodeId);
						}
						this.openNodes.add(nodeId);
						this.myMap.addNode(nodeId, MapAttribute.open);
						this.myMap.addEdge(myPosition, nodeId);
						if(!edges.contains(nodeId+","+myPosition)) {
							this.edges.add(myPosition+","+nodeId);
						}
					}else{
						if(!wind && !nodeId.equals(myPosition) && dangerousNodes.contains(nodeId)) {
							dangerousNodes.remove(nodeId);
						}
						//the node exist, but not necessarily the edge
						this.myMap.addEdge(myPosition, nodeId);
						if(!edges.contains(nodeId+","+myPosition)) {
							this.edges.add(myPosition+","+nodeId);
						}
					}
					if (nextNode==null && !dangerousNodes.contains(nodeId) && !closedNodes.contains(nodeId) && count_deadlock==0) nextNode=nodeId;
				}
			}
			
			//3) while openNodes is not empty, continues.
			if (this.openNodes.isEmpty()){
				//Explo finished
				finished=true;
				System.out.println(goldNodes);
				System.out.println(diamondNodes);
				System.out.println("Exploration successufully done, behaviour removed.");
				myAgent.addBehaviour(new CheckGoldBehaviour(myAgent, goldNodes, diamondNodes,  myMap, receivers, dangerousNodes));
			}else{
				//4) select next move.
				//4.1 If there exist one open node directly reachable, go for it,
				//	 otherwise choose one from the openNode list, compute the shortestPath and go for it
				if (nextNode==null){
					//no directly accessible openNode
					//chose one, compute the path and take the first step.
					nextNode="";
					List<String> list=null;
					for(String n : openNodes) {
						list=null;
						try {
							if(!n.equals(myPosition) && !dangerousNodes.contains(n)) {
								list=this.myMap.getShortestPath(myPosition, n);
							}
						}catch(IndexOutOfBoundsException e) {
						}
						if(list!=null && list.size()>0) {
							if(!dangerousNodes.contains(list.get(0))) {
								nextNode=list.get(0);
							}
						}
					}
				}
				if(!nextNode.equals("")) {
					if(!((AbstractDedaleAgent)this.myAgent).moveTo(nextNode)) {
						myAgent.addBehaviour(new SendMessageDeadlockBehaviour((AbstractDedaleAgent)myAgent, nextNode.toString(), receivers));
						myAgent.addBehaviour(new ReceiveMessageDeadlockBehaviour((AbstractDedaleAgent)myAgent, nextNode.toString(), this));
						count_deadlock+=1;
						if(count_deadlock>=5) {
							changeDestination();
						}
						if(count_deadlock>=10) {
							moveFreeNode(myPosition, nextNode);
						}
					}else {
						count_deadlock=0;
					}
				}
			}

		}
	}

	@Override
	public boolean done() {
		return finished;
	}
	
	public void changeDestination() {
		Collections.shuffle(this.openNodes);
	}
	
	public void addGoldInfo(Couple<String,List<Couple<Observation,Integer>>> obs){
		ArrayList<String> l=new ArrayList<>();
		l.add(obs.getLeft());
		l.add("");
		l.add("");
		l.add("");
		for(Couple<Observation,Integer> c : obs.getRight()) {
			if(c.getLeft().equals(Observation.GOLD)) {
				l.set(1, String.valueOf(c.getRight()));
			}
			if(c.getLeft().equals(Observation.STRENGH)) {
				l.set(2, String.valueOf(c.getRight()));
			}
			if(c.getLeft().equals(Observation.LOCKPICKING)) {
				l.set(3, String.valueOf(c.getRight()));
			}
		}
		if(!goldNodes.contains(l)) {
			goldNodes.add(l);
		}
	}
	
	public void addDiamondInfo(Couple<String,List<Couple<Observation,Integer>>> obs){
		ArrayList<String> l=new ArrayList<>();
		l.add(obs.getLeft());
		l.add("");
		l.add("");
		l.add("");
		for(Couple<Observation,Integer> c : obs.getRight()) {
			if(c.getLeft().equals(Observation.DIAMOND)) {
				l.set(1, String.valueOf(c.getRight()));
			}
			if(c.getLeft().equals(Observation.STRENGH)) {
				l.set(2, String.valueOf(c.getRight()));
			}
			if(c.getLeft().equals(Observation.LOCKPICKING)) {
				l.set(3, String.valueOf(c.getRight()));
			}
		}
		if(!diamondNodes.contains(l)) {
			diamondNodes.add(l);
		}
	}
	
	private void moveFreeNode(String myPosition, String nextNode) {
		List<Couple<String,List<Couple<Observation,Integer>>>> lobs;
		try {
			lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
		}catch(ConcurrentModificationException e){
			lobs=new ArrayList<>();
		}
		if(lobs.size()>0) {
			for(Couple<String, List<Couple<Observation,Integer>>> c : lobs) {
				if(!c.getLeft().equals(myPosition) && !c.getLeft().equals(nextNode) && !dangerousNodes.contains(c.getLeft())) {
					if(myAgent.moveTo(c.getLeft())) {
						break;
					}
				}
			}
		}
	}

}
