package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class ReceiveMapInfoBehaviour extends SimpleBehaviour{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean finished;
	/**
	 * Current knowledge of the agent regarding the environment
	 */
	private MapRepresentation myMap;

	/**
	 * Nodes known but not yet visited
	 */
	private ArrayList<String> openNodes;
	/**
	 * Visited nodes
	 */
	private HashSet<String> closedNodes;
	/**
	 * edges
	 */
	private HashSet<String> edges;
	
	private ArrayList<String> dangerousNodes;
	
	public ReceiveMapInfoBehaviour(final AbstractDedaleAgent myAgent, MapRepresentation map, ArrayList<String> openNodes, HashSet<String> closedNodes, HashSet<String> edges, ArrayList<String> dangerousNodes) {
		super(myAgent);
		this.myMap=map;
		this.edges=edges;
		this.openNodes=openNodes;
		this.closedNodes=closedNodes;
		this.dangerousNodes=dangerousNodes;
	}
	
	
	@Override
	public void action() {
		final MessageTemplate msgTemplateClosed = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),MessageTemplate.MatchProtocol("MAPINFOCLOSED"));
		final ACLMessage msgClosed = this.myAgent.receive(msgTemplateClosed);
		if(msgClosed != null) {
			String closed[]= msgClosed.getContent().split(",");
			for(int i=0; i<closed.length -1; i++) {
				myMap.addNode(closed[i]);
				closedNodes.add(closed[i]);
				if(openNodes.contains(closed[i])) {
					openNodes.remove(closed[i]);
				}
				if(dangerousNodes.contains(closed[i])) {
					dangerousNodes.remove(closed[i]);
				}
			}
		}
		final MessageTemplate msgTemplateOpen = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),MessageTemplate.MatchProtocol("MAPINFOOPEN"));
		final ACLMessage msgOpen = this.myAgent.receive(msgTemplateOpen);
		if(msgOpen != null) {
			try {
				if(msgOpen.getContentObject() instanceof ArrayList) {
					ArrayList<String> open= (ArrayList<String>) msgOpen.getContentObject();
					for(String n : open) {
						if(!closedNodes.contains(n) && !openNodes.contains(n)) {
							myMap.addNode(n, MapRepresentation.MapAttribute.open);
							this.openNodes.add(n);
						}
					}
				}
			} catch (UnreadableException e) {
				e.printStackTrace();
			}
		}

		final MessageTemplate msgTemplateEdges = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),MessageTemplate.MatchProtocol("MAPINFOEDGES"));
		final ACLMessage msgEdges = this.myAgent.receive(msgTemplateEdges);
		if(msgEdges != null) {
			try {
				if(msgEdges.getContentObject() instanceof HashSet) {
					HashSet<String> edges= (HashSet<String>) msgEdges.getContentObject();
					for(String e : edges) {
						String tab[]=e.split(",");
						if(!closedNodes.contains(tab[0]) && !openNodes.contains(tab[0])) {
							openNodes.add(tab[0]);
							myMap.addNode(tab[0]);
						}
						if(!closedNodes.contains(tab[1]) && !openNodes.contains(tab[1])) {
							openNodes.add(tab[1]);
							myMap.addNode(tab[1]);
						}
						myMap.addEdge(tab[0], tab[1]);
						if(!edges.contains(tab[1]+","+tab[0])) {
							edges.add(e);
						}
					}
				}
			} catch (UnreadableException e) {
				e.printStackTrace();
			}
		}
		final MessageTemplate msgTemplateDanger = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),MessageTemplate.MatchProtocol("MAPINFODANGER"));
		final ACLMessage msgDanger = this.myAgent.receive(msgTemplateDanger);
		if(msgDanger != null) {
			try {
				if(msgDanger.getContentObject() instanceof ArrayList) {
					ArrayList<String> danger= (ArrayList<String>) msgDanger.getContentObject();
					for(String d : danger) {
						if(!dangerousNodes.contains(d) && !closedNodes.contains(d)) {
							dangerousNodes.add(d);
						}
					}
				}
			} catch (UnreadableException e) {
				e.printStackTrace();
			}
		}
		
		finished=openNodes.isEmpty();
	}


	@Override
	public boolean done() {
		return finished;
	}

}
