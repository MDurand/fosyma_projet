package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.SimpleBehaviour;

public class CheckGoldBehaviour extends SimpleBehaviour implements IDeadlockMultiAgentBehaviour, IChestOpeningBehaviour{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;
	
	private ArrayList<ArrayList<String>> goldNodes;
	private ArrayList<ArrayList<String>> diamondNodes;
	private MapRepresentation myMap;
	private ArrayList<String> dangerousNodes;
	private List<String> receivers;
	
	private String obj;
	
	private AbstractDedaleAgent myAgent;
	
	private SendGoldInfoBehaviour sgib;
	
	private Integer strength;
	private Integer lockPickingSkill;
	
	private int count_deadlock;
	
	public CheckGoldBehaviour(AbstractDedaleAgent myAgent, ArrayList<ArrayList<String>> goldNodes, ArrayList<ArrayList<String>> diamondNodes, MapRepresentation myMap, List<String> receivers, ArrayList<String> dangerousNodes) {
		super(myAgent);
		this.myAgent=myAgent;
		this.goldNodes=goldNodes;
		this.diamondNodes=diamondNodes;
		this.myMap=myMap;
		this.receivers=receivers;
		this.dangerousNodes=dangerousNodes;
		count_deadlock=0;
		Set<Couple<Observation,Integer>> s=myAgent.getMyExpertise();
		for(Couple<Observation,Integer> c : s) {
			if(c.getLeft().equals(Observation.STRENGH)) {
				this.strength=c.getRight();
			}
			if(c.getLeft().equals(Observation.LOCKPICKING)) {
				this.lockPickingSkill=c.getRight();
			}
		}
		
	}
	
	@Override
	public void action() {
		sgib=new SendGoldInfoBehaviour(myAgent, goldNodes, diamondNodes, receivers);
		myAgent.addBehaviour(sgib);
		this.myAgent.addBehaviour(new SendDangerInfoBehaviour(myAgent, dangerousNodes, receivers));
		myAgent.addBehaviour(new ReceiveAskHelpChestBehaviour(myAgent, strength, lockPickingSkill, this));
		//0) Retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		if (myPosition!=null){
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs;
			try {
				lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			}catch(ConcurrentModificationException e){
				lobs=new ArrayList<>();
			}
			if(lobs.size()>0) {
				if(myPosition.equals(obj)) {
					boolean still_here=false;
					for(Couple<String,List<Couple<Observation,Integer>>> c : lobs) {
						if(c.getLeft().equals(obj)) {
							for(Couple<Observation,Integer> C : c.getRight()) {
								if(C.getLeft().equals(Observation.GOLD) || C.getLeft().equals(Observation.DIAMOND)) {
									still_here=true;
									break;
								}
							}
						}
					}
					if(!still_here) {
						ArrayList<String> n=null;
						for(ArrayList<String> b : goldNodes) {
							if(b.get(0).equals(myPosition)) {
								n=b;
								break;
							}
						}
						if(n!=null) {
							goldNodes.remove(n);
						}
					}
					obj=null;
				}
			}
			
			/**
			 * Just added here to let you see what the agent is doing, otherwise he will be too quick
			 */
			try {
				this.myAgent.doWait(500);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (this.goldNodes.isEmpty() && this.diamondNodes.isEmpty()){
				//Explo finished
				finished=true;
				System.out.println("No more gold nodes.");
			}else{
				if (obj==null){
					obj=chooseNewObj();
				}
				String nextNode="";
				List<String> l=new ArrayList<>();
				if(!myPosition.equals(obj)) {
					l=myMap.getShortestPath(myPosition, obj);
				}
				if(l.size()>0) {
					nextNode=l.get(0);
				}
				if(dangerousNodes.contains(nextNode)) {
					nextNode="";
					obj=chooseNewObj();
				}
				if(!nextNode.equals("")) {
					if(!((AbstractDedaleAgent)this.myAgent).moveTo(nextNode)) {
						myAgent.addBehaviour(new SendMessageDeadlockBehaviour((AbstractDedaleAgent)myAgent, nextNode.toString(), receivers));
						myAgent.addBehaviour(new ReceiveMessageDeadlockBehaviour((AbstractDedaleAgent)myAgent, nextNode.toString(), this));
						count_deadlock+=1;
						if(count_deadlock>5) {
							changeDestination();
						}
						if(count_deadlock>=10) {
							moveFreeNode(myPosition, nextNode);
						}
					}else {
						count_deadlock=0;
					}
				}
			}
		}
	}

	private String chooseNewObj() {
		Random r=new Random();
		int i=r.nextInt(goldNodes.size()+diamondNodes.size());
		if(i<goldNodes.size()) {
			ArrayList<String> node=goldNodes.get(0);
			obj=node.get(0);
			goldNodes.remove(node);
			goldNodes.add(node);
			return obj;
		}else {
			ArrayList<String> node=diamondNodes.get(0);
			obj=node.get(0);
			diamondNodes.remove(node);
			diamondNodes.add(node);
			return obj;
		}
		
	}
	@Override
	public boolean done() {
		return finished;
	}

	@Override
	public void changeDestination() {
		obj=chooseNewObj();
	}
	
	public void addGoldInfo(Couple<String,List<Couple<Observation,Integer>>> obs){
		ArrayList<String> l=new ArrayList<>();
		l.add(obs.getLeft());
		l.add("");
		l.add("");
		l.add("");
		for(Couple<Observation,Integer> c : obs.getRight()) {
			if(c.getLeft().equals(Observation.GOLD)) {
				l.set(1, String.valueOf(c.getRight()));
			}
			if(c.getLeft().equals(Observation.STRENGH)) {
				l.set(2, String.valueOf(c.getRight()));
			}
			if(c.getLeft().equals(Observation.LOCKPICKING)) {
				l.set(3, String.valueOf(c.getRight()));
			}
		}
		if(!goldNodes.contains(l)) {
			goldNodes.add(l);
		}
	}
	
	public void addDiamondInfo(Couple<String,List<Couple<Observation,Integer>>> obs){
		ArrayList<String> l=new ArrayList<>();
		l.add(obs.getLeft());
		l.add("");
		l.add("");
		l.add("");
		for(Couple<Observation,Integer> c : obs.getRight()) {
			if(c.getLeft().equals(Observation.DIAMOND)) {
				l.set(1, String.valueOf(c.getRight()));
			}
			if(c.getLeft().equals(Observation.STRENGH)) {
				l.set(2, String.valueOf(c.getRight()));
			}
			if(c.getLeft().equals(Observation.LOCKPICKING)) {
				l.set(3, String.valueOf(c.getRight()));
			}
		}
		if(!diamondNodes.contains(l)) {
			diamondNodes.add(l);
		}
	}
	
	private void moveFreeNode(String myPosition, String nextNode) {
		List<Couple<String,List<Couple<Observation,Integer>>>> lobs;
		try {
			lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
		}catch(ConcurrentModificationException e){
			lobs=new ArrayList<>();
		}
		if(lobs.size()>0) {
			for(Couple<String, List<Couple<Observation,Integer>>> c : lobs) {
				if(!c.getLeft().equals(myPosition) && !c.getLeft().equals(nextNode) && !dangerousNodes.contains(c.getLeft())) {
					if(myAgent.moveTo(c.getLeft())) {
						break;
					}
				}
			}
		}
	}

	@Override
	public void setObjective(String node) {
		obj=node;
		myAgent.addBehaviour(new ReceiveStopHelpBehaviour(myAgent, this));
	}
	
	public void stopHelp() {
		obj=chooseNewObj();
	}

}
