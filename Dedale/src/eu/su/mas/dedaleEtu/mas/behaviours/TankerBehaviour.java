package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.behaviours.SimpleBehaviour;

public class TankerBehaviour extends SimpleBehaviour implements IDeadlockMultiAgentBehaviour, IChestOpeningBehaviour{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;
	
	private MapRepresentation myMap;
	
	private List<String> receivers;
	
	private AbstractDedaleAgent myAgent;
	
	private ArrayList<String> openNodes;
	private HashSet<String> closedNodes;
	private HashSet<String> edges;
	private ArrayList<String> dangerousNodes;
	
	private int count;
	
	private String obj;
	private String originalPosition;
	
	private Integer strength;
	private Integer lockPickingSkill;
	
	public TankerBehaviour(AbstractDedaleAgent myAgent, List<String> receivers) {
		super(myAgent);
		this.myAgent=myAgent;
		myMap=null;
		this.receivers=receivers;
		originalPosition=null;
		
		Set<Couple<Observation,Integer>> s=myAgent.getMyExpertise();
		for(Couple<Observation,Integer> c : s) {
			if(c.getLeft().equals(Observation.STRENGH)) {
				this.strength=c.getRight();
			}
			if(c.getLeft().equals(Observation.LOCKPICKING)) {
				this.lockPickingSkill=c.getRight();
			}
		}
		
		openNodes=new ArrayList<>();
		closedNodes=new HashSet<>();
		edges=new HashSet<>();
		dangerousNodes=new ArrayList<>();
		count=0;
		
	}

	@Override
	public void action() {
		count-=1;
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		if(this.myMap==null) {
			myMap=new MapRepresentation();
			this.originalPosition=myPosition;
			obj=this.originalPosition;
		}
		myAgent.addBehaviour(new SendTankerPositionBehaviour(myAgent, receivers, originalPosition));
		myAgent.addBehaviour(new ReceiveMessageDeadlockBehaviour(myAgent, originalPosition, this));
		myAgent.addBehaviour(new ReceiveAskHelpChestBehaviour(myAgent, strength, lockPickingSkill, this));
		myAgent.addBehaviour(new ReceiveMapInfoBehaviour(myAgent, myMap, openNodes, closedNodes, edges, dangerousNodes));
		
		if (myPosition!=null){
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs;
			try {
				lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			}catch(ConcurrentModificationException e){
				lobs=new ArrayList<>();
			}
			/**
			 * Just added here to let you see what the agent is doing, otherwise he will be too quick
			 */
			try {
				this.myAgent.doWait(500);
			} catch (Exception e) {
				e.printStackTrace();
			}
			boolean wind=false;
			
			//1) remove the current node from openlist and add it to closedNodes.
			if(lobs.size()>0) {
				this.closedNodes.add(myPosition);
				this.openNodes.remove(myPosition);
				
				this.myMap.addNode(myPosition);
				for(Couple<String, List<Couple<Observation, Integer>>> c : lobs) {
					if(c.getLeft().equals(myPosition)) {
						for(Couple<Observation, Integer> C : c.getRight()) {
							if(C.getLeft().equals(Observation.WIND)) {
								wind=true;
							}
						}
						
					}
				}
			}
			//2) get the surrounding nodes and, if not in closedNodes, add them to open nodes.
			String nextNode=null;
			Iterator<Couple<String, List<Couple<Observation, Integer>>>> iter=lobs.iterator();
			while(iter.hasNext()){
				Couple<String, List<Couple<Observation, Integer>>> node=iter.next();
				String nodeId=node.getLeft();
				if (!this.closedNodes.contains(nodeId)){
					if (!this.openNodes.contains(nodeId)) {
						if(wind && !nodeId.equals(myPosition)){
							dangerousNodes.add(nodeId);
						}
						this.openNodes.add(nodeId);
						this.myMap.addNode(nodeId, MapAttribute.open);
						this.myMap.addEdge(myPosition, nodeId);
						if(!edges.contains(nodeId+","+myPosition)) {
							this.edges.add(myPosition+","+nodeId);
						}
					}else{
						if(!wind && !nodeId.equals(myPosition) && dangerousNodes.contains(nodeId)) {
							dangerousNodes.remove(nodeId);
						}
						//the node exist, but not necessarily the edge
						this.myMap.addEdge(myPosition, nodeId);
						if(!edges.contains(nodeId+","+myPosition)) {
							this.edges.add(myPosition+","+nodeId);
						}
					}
				}
			}
			if(!myPosition.equals(originalPosition) && count<=0) {
				List<String>list=this.myMap.getShortestPath(myPosition, originalPosition);
				if(list!=null && list.size()>0) {
					if(!dangerousNodes.contains(list.get(0))) {
						nextNode=list.get(0);
					}
				}
				myAgent.moveTo(nextNode);
			}else {
				if(!obj.equals(originalPosition)) {
					List<String>list=null;
					try {
						list=this.myMap.getShortestPath(myPosition, obj);
					}catch(Exception e){
					}
					if(list!=null && list.size()>0) {
						if(!dangerousNodes.contains(list.get(0))) {
							nextNode=list.get(0);
							myAgent.moveTo(nextNode);
						}
					}
				}
			}
		}

	}

	@Override
	public boolean done() {
		return finished;
	}

	@Override
	public void changeDestination() {
		count=5;
		Collections.shuffle(openNodes);
		String nextNode="";
		List<String> list=null;
		String myPosition=myAgent.getCurrentPosition();
		for(String n : openNodes) {
			list=null;
			try {
				if(!n.equals(myPosition) && !dangerousNodes.contains(n)) {
					list=this.myMap.getShortestPath(myPosition, n);
				}
			}catch(IndexOutOfBoundsException e) {
			}
			if(list!=null && list.size()>0) {
				if(!dangerousNodes.contains(list.get(0))) {
					nextNode=list.get(0);
					break;
				}
			}
		}
		myAgent.addBehaviour(new SendMapInfoBehaviour(myAgent, closedNodes, openNodes, edges, receivers));
		if(nextNode!=null) {
			myAgent.moveTo(nextNode);
		}
	}

	@Override
	public void setObjective(String node) {
		obj=node;
		myAgent.addBehaviour(new ReceiveStopHelpBehaviour(myAgent, this));
	}
	
	public void stopHelp() {
		obj=originalPosition;
	}

}
