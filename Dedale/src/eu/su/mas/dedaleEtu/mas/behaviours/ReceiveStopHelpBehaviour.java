package eu.su.mas.dedaleEtu.mas.behaviours;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ReceiveStopHelpBehaviour extends SimpleBehaviour{

	private static final long serialVersionUID = 1L;
	private boolean finished;
	
	private IChestOpeningBehaviour b;
	
	private Integer str;
	private Integer lp;
	
	private AbstractDedaleAgent myAgent;
	
	public ReceiveStopHelpBehaviour(AbstractDedaleAgent myAgent, IChestOpeningBehaviour b) {
		super(myAgent);
		this.myAgent=myAgent;
		this.b=b;
	}

	@Override
	public void action() {
		final MessageTemplate msgTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
		MessageTemplate.MatchProtocol("STOPHELP"));			

		final ACLMessage msg = this.myAgent.receive(msgTemplate);
		if (msg != null) {	
			String info=msg.getContent();
			if(info.equals("STOP")) {
				b.stopHelp();
				finished=true;
			}
		}
	}

	@Override
	public boolean done() {
		return finished;
	}


}
