package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;

public class SendTankerPositionBehaviour extends SimpleBehaviour{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;

	private List<String> receivers;
	
	private String node;
	private AbstractDedaleAgent myAgent;
	
	public SendTankerPositionBehaviour(AbstractDedaleAgent myAgent, List<String> receivers, String node) {
		super(myAgent);
		this.myAgent=myAgent;
		this.receivers=receivers;
		this.node=node;
	}
	
	@Override
	public void action() {
		final ACLMessage msgTanker = new ACLMessage(ACLMessage.INFORM);
		msgTanker.setSender(this.myAgent.getAID());
		for(String receiver : receivers) {
			msgTanker.addReceiver(new AID(receiver, AID.ISLOCALNAME));
		}
		msgTanker.setProtocol("TANKERPOSITION");
		msgTanker.setContent(node);
		((AbstractDedaleAgent) this.myAgent).sendMessage(msgTanker);

		finished=true;
	}

	@Override
	public boolean done() {
		return finished;
	}

}
