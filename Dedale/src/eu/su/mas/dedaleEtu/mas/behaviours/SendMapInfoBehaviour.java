package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;

public class SendMapInfoBehaviour extends SimpleBehaviour {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;
	
	/**
	 * Nodes known but not yet visited
	 */
	private ArrayList<String> openNodes;
	/**
	 * Visited nodes
	 */
	private HashSet<String> closedNodes;
	/**
	 * edges
	 */
	private HashSet<String> edges;
	
	private List<String> receivers;

	public SendMapInfoBehaviour(AbstractDedaleAgent myAgent, HashSet<String> closedNodes, ArrayList<String> openNodes, HashSet<String> edges, List<String> receivers) {
		super(myAgent);
		this.edges=edges;
		this.openNodes=openNodes;
		this.closedNodes=closedNodes;
		this.receivers=new ArrayList<>();
		for(String r : receivers) {
			this.receivers.add(r);
		}
	}
	
	
	@Override
	public void action() {
		//Création des messages
		final ACLMessage msgClosed1 = new ACLMessage(ACLMessage.INFORM);
		final ACLMessage msgOpen = new ACLMessage(ACLMessage.INFORM);
		final ACLMessage msgEdges = new ACLMessage(ACLMessage.INFORM);
		
		//Ajout de l'envoyeur
		msgClosed1.setSender(this.myAgent.getAID());
		msgOpen.setSender(this.myAgent.getAID());
		msgEdges.setSender(this.myAgent.getAID());
		
		//Ajout des recepteurs
		for(String receiver : receivers) {
			msgClosed1.addReceiver(new AID(receiver, AID.ISLOCALNAME));
			msgOpen.addReceiver(new AID(receiver, AID.ISLOCALNAME));
			msgEdges.addReceiver(new AID(receiver, AID.ISLOCALNAME));
		}
		
		//Définition des protocoles
		msgClosed1.setProtocol("MAPINFOCLOSED");
		msgOpen.setProtocol("MAPINFOOPEN");
		msgEdges.setProtocol("MAPINFOEDGES");
		
		//Ajout du contenu
		try {
			String closed1="";
			for(String n : closedNodes) {
				closed1+=n+",";
			}
			msgClosed1.setContent(closed1);
			msgOpen.setContentObject(openNodes);
			msgEdges.setContentObject(edges);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Envoi
		if(this.myAgent!=null) {
			try {
				((AbstractDedaleAgent) this.myAgent).sendMessage(msgEdges);
				((AbstractDedaleAgent) this.myAgent).sendMessage(msgClosed1);
				((AbstractDedaleAgent) this.myAgent).sendMessage(msgOpen);
			}catch(NullPointerException e) {
			}
		}
		finished=true;
	}

	@Override
	public boolean done() {
		return finished;
	}

}
