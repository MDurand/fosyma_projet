package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.behaviours.SimpleBehaviour;

public class CollectBehaviour extends SimpleBehaviour implements IDeadlockMultiAgentBehaviour{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;
	
	private ArrayList<String> openNodes;
	private HashSet<String> closedNodes;
	private HashSet<String> edges;
	private ArrayList<String> emptyNodes;
	
	private ArrayList<String> dangerousNodes;
	
	private String obj;
	private ArrayList<ArrayList<String>> goldNodes;
	private ArrayList<ArrayList<String>> diamondNodes;
	private MapRepresentation myMap;
	private List<String> receivers;
	private String tankerPosition;
	private final AbstractDedaleAgent myAgent;
	
	private final String nameTanker;
	
	private ReceiveTankerPositionBehaviour rtpb;
		
	private int backpackCapacity;
	private int strength;
	private int lockPickingSkill;
	
	private boolean hard=false;
	ArrayList<String> hardNode=null;
	
	private int countDeadlock;

	public CollectBehaviour(AbstractDedaleAgent myAgent, List<String> receivers, String nameTanker) {
		super(myAgent);
		this.myAgent=myAgent;
		this.receivers=receivers;
		this.nameTanker=nameTanker;
		Set<Couple<Observation,Integer>> s=myAgent.getMyExpertise();
		for(Couple<Observation,Integer> c : s) {
			if(c.getLeft().equals(Observation.STRENGH)) {
				this.strength=c.getRight();
			}
			if(c.getLeft().equals(Observation.LOCKPICKING)) {
				this.lockPickingSkill=c.getRight();
			}
		}
		this.backpackCapacity=myAgent.getBackPackFreeSpace();
		openNodes=new ArrayList<>();
		closedNodes=new HashSet<>();
		dangerousNodes=new ArrayList<>();
		edges=new HashSet<>();
		goldNodes=new ArrayList<>();
		diamondNodes=new ArrayList<>();
		emptyNodes=new ArrayList<>();
		
		countDeadlock=0;
	}
	
	@Override
	public void action() {
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		if(myMap==null) {
			this.myMap=new MapRepresentation();
			myAgent.addBehaviour(new ReceiveMapInfoBehaviour(myAgent, myMap, openNodes, closedNodes, edges, dangerousNodes));
			myAgent.addBehaviour(new ReceiveGoldInfoBehaviour(myAgent, goldNodes, diamondNodes, emptyNodes));
			rtpb=new ReceiveTankerPositionBehaviour(myAgent);
			myAgent.addBehaviour(rtpb);
		}
		if(tankerPosition==null) {
			tankerPosition=rtpb.getPosition();
		}else {
			myAgent.addBehaviour(new SendTankerPositionBehaviour(myAgent, receivers, tankerPosition));
		}
		if(myAgent.getBackPackFreeSpace()!=backpackCapacity) {
			boolean emp=myAgent.emptyMyBackPack(nameTanker);
			if(emp) {
				chooseNewObj();
			}
		}
		for(String e : emptyNodes) {
			ArrayList<String> el=null;
			for(ArrayList<String> goldNode : goldNodes) {
				if(goldNode.get(0).equals(e)) {
					el=goldNode;
				}
			}
			if(el!=null) {
				goldNodes.remove(el);
			}
			el=null;
			for(ArrayList<String> diamondNode : goldNodes) {
				if(diamondNode.get(0).equals(e)) {
					el=diamondNode;
				}
			}
			if(el!=null) {
				diamondNodes.remove(el);
			}
		}
		if(hard) {
			myAgent.addBehaviour(new AskHelpChestBehaviour(myAgent, hardNode.get(0),Integer.valueOf(hardNode.get(2))-strength, Integer.valueOf(hardNode.get(3))-lockPickingSkill, receivers));
		}
		if (myPosition!=null){
			//List of observable from the agent's current position
			List<Couple<String,List<Couple<Observation,Integer>>>> lobs;
			try {
				lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
			}catch(ConcurrentModificationException e){
				lobs=new ArrayList<>();
			}
			boolean wind=false;
			if(lobs.size()>0) {
				this.closedNodes.add(myPosition);
				this.openNodes.remove(myPosition);
				
				this.myMap.addNode(myPosition);
				for(Couple<String, List<Couple<Observation, Integer>>> c : lobs) {
					if(c.getLeft().equals(myPosition)) {
						for(Couple<Observation, Integer> C : c.getRight()) {
							if(C.getLeft().equals(Observation.WIND)) {
								wind=true;
							}
						}
						
					}
				}
				
				//2) get the surrounding nodes and, if not in closedNodes, add them to open nodes.
				Iterator<Couple<String, List<Couple<Observation, Integer>>>> iter=lobs.iterator();
				while(iter.hasNext()){
					Couple<String, List<Couple<Observation, Integer>>> node=iter.next();
					String nodeId=node.getLeft();
					List<Couple<Observation, Integer>> obs =node.getRight();
					for(Couple<Observation, Integer> c : obs) {
						if(c.getLeft().equals(Observation.GOLD)) {
							if(emptyNodes.contains(nodeId)) {
								emptyNodes.remove(nodeId);
							}
							addGoldInfo(node);
						}
						if(c.getLeft().equals(Observation.DIAMOND)) {
							if(emptyNodes.contains(nodeId)) {
								emptyNodes.remove(nodeId);
							}
							addDiamondInfo(node);
						}
					}
					if (!this.closedNodes.contains(nodeId)){
						if (!this.openNodes.contains(nodeId)) {
							if(wind && !nodeId.equals(myPosition)){
								dangerousNodes.add(nodeId);
							}
							this.openNodes.add(nodeId);
							this.myMap.addNode(nodeId, MapAttribute.open);
							this.myMap.addEdge(myPosition, nodeId);
							if(!edges.contains(nodeId+","+myPosition)) {
								this.edges.add(myPosition+","+nodeId);
							}
						}else{
							if(!wind && !nodeId.equals(myPosition) && dangerousNodes.contains(nodeId)) {
								dangerousNodes.remove(nodeId);
							}
							//the node exist, but not necessarily the edge
							this.myMap.addEdge(myPosition, nodeId);
							if(!edges.contains(nodeId+","+myPosition)) {
								this.edges.add(myPosition+","+nodeId);
							}
						}
					}
				}
				int i=-1;
				ArrayList<String> treasureNode=null;
				if(myPosition.equals(obj) && backpackCapacity==myAgent.getBackPackFreeSpace()) {
					for(ArrayList<String> node : goldNodes) {
						if(node.get(0).equals(myPosition)) {
							treasureNode=node;
							if(hard) {
								if(myAgent.openLock(Observation.GOLD)){
									node.set(2,"0");
									node.set(3,"0");
									treasureNode.set(2,"0");
									treasureNode.set(3,"0");
									i=myAgent.pick();
								}
								if(i>0) {
									hard=false;
									hardNode=null;
									myAgent.addBehaviour(new SendStopHelpBehaviour(myAgent, receivers));										
								}
							}else {
								if(Integer.valueOf(node.get(2))<=strength) {
									if(Integer.valueOf(node.get(3))<=lockPickingSkill) {
										if(myAgent.openLock(Observation.GOLD)){
											node.set(2,"0");
											node.set(3,"0");
											treasureNode.set(2,"0");
											treasureNode.set(3,"0");
										}
										i=myAgent.pick();
									}
								}
							}
							if(i==0) {
								chooseNewObj();
								if(!emptyNodes.contains(treasureNode.get(0))){
									emptyNodes.add(treasureNode.get(0));
									System.out.println(emptyNodes);
								}
							}
						}
					}
					if(i==0) {
						goldNodes.remove(treasureNode);
						System.out.println(goldNodes);
					}
					for(ArrayList<String> node : diamondNodes) {
						if(node.get(0).equals(myPosition)) {
							treasureNode=node;
							if(hard) {
								if(myAgent.openLock(Observation.DIAMOND)){
									node.set(2,"0");
									node.set(3,"0");
									treasureNode.set(2,"0");
									treasureNode.set(3,"0");
									i=myAgent.pick();
									hard=false;
									hardNode=null;
								}
							}else {
								if(Integer.valueOf(node.get(2))<=strength) {
									if(Integer.valueOf(node.get(3))<=lockPickingSkill) {
										if(myAgent.openLock(Observation.DIAMOND)){
											node.set(2,"0");
											node.set(3,"0");
											treasureNode.set(2,"0");
											treasureNode.set(3,"0");
											i=myAgent.pick();
										}else {
											i=0;
										}
									}
								}
							}
							if(i==0) {
								chooseNewObj();
								if(!emptyNodes.contains(treasureNode.get(0))){
									emptyNodes.add(treasureNode.get(0));
									System.out.println(emptyNodes);
								}
							}
						}
					}
					if(i==0) {
						diamondNodes.remove(treasureNode);
						System.out.println("Diamonds");
						System.out.println(diamondNodes);
					}
					obj=null;
				}
			}
			
			/**
			 * Just added here to let you see what the agent is doing, otherwise he will be too quick
			 */
			try {
				this.myAgent.doWait(500);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (this.goldNodes.isEmpty() && myAgent.getBackPackFreeSpace()==backpackCapacity && openNodes.isEmpty()){
				//Explo finished
				finished=true;
				System.out.println("No more gold nodes.");
			}else{
				if(obj==null) {
					obj=chooseNewObj();
				}
			}
			List<String> l=new ArrayList<>();
			if(obj!=null && !obj.equals(myPosition)) {
				try {
					l=myMap.getShortestPath(myPosition, obj);
				}catch(Exception e) {
				}
				String nextNode="";
				if(l.size()>0) {
					nextNode=l.get(0);
				}
				if(!nextNode.equals("")) {
					if(!((AbstractDedaleAgent)this.myAgent).moveTo(nextNode)) {
						countDeadlock+=1;
						myAgent.addBehaviour(new SendMessageDeadlockBehaviour((AbstractDedaleAgent)myAgent, nextNode.toString(), receivers));
						myAgent.addBehaviour(new ReceiveMessageDeadlockBehaviour((AbstractDedaleAgent)myAgent, nextNode.toString(), this));
						if(countDeadlock>=5) {
							chooseNewObj();
						}
						if(countDeadlock>=10 && !obj.equals(tankerPosition)) {
							moveFreeNode(myPosition, nextNode);
						}
					}else {
						countDeadlock=0;
					}
				}
			}
		}

	}

	@Override
	public boolean done() {
		return finished;
	}

	public String chooseNewObj() {
		if(goldNodes.size()>0 && myAgent.getBackPackFreeSpace()==backpackCapacity && myAgent.getMyTreasureType().equals(Observation.GOLD)) {
			for(ArrayList<String> node : goldNodes) {
				if(Integer.valueOf(node.get(3))<=lockPickingSkill && Integer.valueOf(node.get(2))<=strength && !emptyNodes.contains(node.get(0))) {
					if(openNodes.contains(node.get(0)) || closedNodes.contains(node.get(0))) {
						obj=node.get(0);
						break;
					}
				}
			}
		}else {
			if(diamondNodes.size()>0 && myAgent.getBackPackFreeSpace()==backpackCapacity && myAgent.getMyTreasureType().equals(Observation.DIAMOND)) {
				for(ArrayList<String> node : diamondNodes) {
					if(Integer.valueOf(node.get(3))<=lockPickingSkill && Integer.valueOf(node.get(2))<=strength && !emptyNodes.contains(node.get(0))) {
						if(openNodes.contains(node.get(0)) || closedNodes.contains(node.get(0))) {
							obj=node.get(0);
							break;
						}
					}
				}
			}
		}
		if(myAgent.getBackPackFreeSpace()!=backpackCapacity) {
			if(openNodes.contains(tankerPosition) || closedNodes.contains(tankerPosition)) {
				obj=tankerPosition;
			}
		}
		if(goldNodes.size()>0 && obj==null && myAgent.getMyTreasureType().equals(Observation.GOLD)) {
			obj=goldNodes.get(0).get(0);
			hard=true;
			hardNode=goldNodes.get(0);
		}
		if(diamondNodes.size()>0 && obj==null && myAgent.getMyTreasureType().equals(Observation.DIAMOND)) {
			obj=diamondNodes.get(0).get(0);
			hard=true;
			hardNode=diamondNodes.get(0);
		}
		if(obj==null) {
			for(String n : openNodes) {
				if(!dangerousNodes.contains(n)){
					obj=n;
				}
			}
		}
		return obj;
	}
	
	@Override
	public void changeDestination() {
		obj=chooseNewObj();
	}
	
	public void addGoldInfo(Couple<String,List<Couple<Observation,Integer>>> obs){
		ArrayList<String> l=new ArrayList<>();
		l.add(obs.getLeft());
		l.add("");
		l.add("");
		l.add("");
		for(Couple<Observation,Integer> c : obs.getRight()) {
			if(c.getLeft().equals(Observation.GOLD)) {
				l.set(1, String.valueOf(c.getRight()));
			}
			if(c.getLeft().equals(Observation.STRENGH)) {
				l.set(2, String.valueOf(c.getRight()));
			}
			if(c.getLeft().equals(Observation.LOCKPICKING)) {
				l.set(3, String.valueOf(c.getRight()));
			}
		}
		if(!goldNodes.contains(l)) {
			goldNodes.add(l);
		}
	}
	
	public void addDiamondInfo(Couple<String,List<Couple<Observation,Integer>>> obs){
		ArrayList<String> l=new ArrayList<>();
		l.add(obs.getLeft());
		l.add("");
		l.add("");
		l.add("");
		for(Couple<Observation,Integer> c : obs.getRight()) {
			if(c.getLeft().equals(Observation.DIAMOND)) {
				l.set(1, String.valueOf(c.getRight()));
			}
			if(c.getLeft().equals(Observation.STRENGH)) {
				l.set(2, String.valueOf(c.getRight()));
			}
			if(c.getLeft().equals(Observation.LOCKPICKING)) {
				l.set(3, String.valueOf(c.getRight()));
			}
		}
		if(!diamondNodes.contains(l)) {
			diamondNodes.add(l);
		}
	}
	
	private void moveFreeNode(String myPosition, String nextNode) {
		List<Couple<String,List<Couple<Observation,Integer>>>> lobs;
		try {
			lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
		}catch(ConcurrentModificationException e){
			lobs=new ArrayList<>();
		}
		if(lobs.size()>0) {
			for(Couple<String, List<Couple<Observation,Integer>>> c : lobs) {
				if(!c.getLeft().equals(myPosition) && !c.getLeft().equals(nextNode) && !dangerousNodes.contains(c.getLeft())) {
					if(myAgent.moveTo(c.getLeft())) {
						break;
					}
				}
			}
		}
	}

}
