package eu.su.mas.dedaleEtu.mas.behaviours;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ReceiveTankerPositionBehaviour extends SimpleBehaviour{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;
	private String position=null;

	public ReceiveTankerPositionBehaviour(final AbstractDedaleAgent myAgent) {
		super(myAgent);
		position=null;
	}

	@Override
	public void action() {
		final MessageTemplate msgTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
																MessageTemplate.MatchProtocol("TANKERPOSITION"));			

		final ACLMessage msg = this.myAgent.receive(msgTemplate);
		if (msg != null) {	
			position=msg.getContent();
		}
	}


	@Override
	public boolean done() {
		return finished;
	}
	
	public String getPosition() {
		finished=(position!=null);
		return position;
	}

}
