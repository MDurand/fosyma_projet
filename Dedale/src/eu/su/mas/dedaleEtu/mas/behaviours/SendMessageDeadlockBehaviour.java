package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.ArrayList;
import java.util.List;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;

public class SendMessageDeadlockBehaviour extends SimpleBehaviour{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean finished;
	private String obj;
	private List<String> receiverList=new ArrayList<>();
	
	public SendMessageDeadlockBehaviour(final AbstractDedaleAgent myAgent, String posObjectif, List<String> receivers) {
		super(myAgent);
		obj=posObjectif;
		for(String r :receivers) {
			receiverList.add(r);
		}
	}
	
	@Override
	public void action() {
		final ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setSender(this.myAgent.getAID());

		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		
		msg.setContent(myPosition+","+obj+","+ myAgent.getAID().toString());
		msg.setProtocol("DEADLOCK");
		for(String receiver : receiverList) {
			msg.addReceiver(new AID(receiver, AID.ISLOCALNAME));
		}
		((AbstractDedaleAgent) this.myAgent).sendMessage(msg);
		finished=true;
	}

	@Override
	public boolean done() {
		return finished;
	}

}
