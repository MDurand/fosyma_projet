package eu.su.mas.dedaleEtu.mas.behaviours;

public interface IChestOpeningBehaviour {
	public void setObjective(String node);
	public void stopHelp();
}
